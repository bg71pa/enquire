<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Email Settings
| -------------------------------------------------------------------
| Configuration of outgoing mail server.
| */

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.mailgun.org';
$config['smtp_port'] = '587';
$config['smtp_timeout'] = '30';
$config['smtp_user'] = 'postmaster@enquirehub.net';
$config['smtp_pass'] = 'b6d1529e2e973da292f440dea96e86b4';
$config['charset'] = 'utf-8';
$config['mailtype'] = 'html';
$config['wordwrap'] = TRUE;
$config['newline'] = "\r\n";

// custom values from CI Bootstrap
$config['from_email'] = "noreply@enquirehub.com";
$config['from_name'] = "enquirehub";
$config['subject_prefix'] = "";

// Mailgun API (to be used in Email Client library)
$config['mailgun'] = array(
	'domain'			=> 'enquirehub.com',
	'private_api_key'		=> 'key-de364b8bf707493c82de4f461c154e4a',
);