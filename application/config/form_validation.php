<?php

/**
 * Config file for form validation
 * Reference: http://www.codeigniter.com/user_guide/libraries/form_validation.html
 * (Under section "Creating Sets of Rules")
 */

$config = array(

  // Sign Up
  'auth/sign_up' => array(
    /*array(
      'field'    => 'first_name',
      'label'    => 'First Name',
      'rules'    => 'required',
    ),
    array(
      'field'    => 'first_name',
      'label'    => 'First Name',
      'rules'    => 'required',
    ),*/
    /*array(
      'field'    => 'group_id',
      'label'    => 'User Type',
      'rules'    => 'required',
    ),*/
    array(
      'field'    => 'username',
      'label'    => 'User Name',
      'rules'    => 'required|is_unique[users.username]',
    ),    
    array(
      'field'    => 'email',
      'label'    => 'Email',
      'rules'    => 'required|valid_email|is_unique[users.email]',
    ),
    array(
      'field'    => 'password',
      'label'    => 'Password',
      'rules'    => 'required|min_length[8]',
    ),
    array(
      'field'    => 'retype_password',
      'label'    => 'Retype Password',
      'rules'    => 'required|matches[password]',
    ),
  ),

  // Login
  'auth/login' => array(
    array(
      'field'    => 'email',
      'label'    => 'Email',
      'rules'    => 'required|valid_email',
    ),
    array(
      'field'    => 'password',
      'label'    => 'Password',
      'rules'    => 'required',
    ),
  ),
  
  // Change Password
  'auth/change_password' => array(    
    array(
      'field'    => 'old',
      'label'    => 'Old password',
      'rules'    => 'required',
    ),
	array(
      'field'    => 'new',
      'label'    => 'Password',
      'rules'    => 'required|min_length[8]',
    ),
    array(
      'field'    => 'new_confirm',
      'label'    => 'Retype Password',
      'rules'    => 'required|matches[new]',
    ),
	
  ),

  // Forgot Password
  'auth/forgot_password' => array(
    array(
      'field'    => 'email',
      'label'    => 'Email',
      'rules'    => 'required|valid_email',
    ),
  ),

  // Reset Password
  'auth/reset_password' => array(
    array(
      'field'    => 'password',
      'label'    => 'Password',
      'rules'    => 'required|min_length[8]',
    ),
    array(
      'field'    => 'retype_password',
      'label'    => 'Retype Password',
      'rules'    => 'required|matches[password]',
    ),
  ),

  // Demo only
  'contact/index' => array(
    array(
      'field'    => 'name',
      'label'    => 'Name',
      'rules'    => 'required',
    ),
    array(
      'field'    => 'email',
      'label'    => 'Email',
      'rules'    => 'required|valid_email',
    ),
    array(
      'field'    => 'subject',
      'label'    => 'Subject',
      'rules'    => 'required',
    ),
    array(
      'field'    => 'message',
      'label'    => 'Message',
      'rules'    => 'required',
      'type'    => 'textarea',
      'rows'    => '2',
    ),
  ),
  'demo/form_bs3' => array(
    array(
      'field'    => 'name',
      'label'    => 'Name',
      'rules'    => 'required',
    ),
    array(
      'field'    => 'email',
      'label'    => 'Email',
      'rules'    => 'required|valid_email',
    ),
    array(
      'field'    => 'subject',
      'label'    => 'Subject',
      'rules'    => 'required',
    ),
    array(
      'field'    => 'message',
      'label'    => 'Message',
      'rules'    => 'required',
    ),
  ),

);

/**
 * Google reCAPTCHA settings:
 * https://www.google.com/recaptcha/
 */
$config['recaptcha'] = array(
  'site_key'    => '6Lc02hAUAAAAAOsLmG3GWhecx-R9mMWu-t6I2iWB', //  '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  'secret_key'  => '6Lc02hAUAAAAAJv53tqf0FCEmoRdGzNZ1XWASEfT' //  '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
);
