<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct()
    {
		parent::__construct();

		// CI Bootstrap libraries
		$this->load->library('form_builder');
		
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
		$this->load->library('system_message');
		$this->load->library('email_client');

		$this->push_breadcrumb('Auth');
    }

    function index()
    {
		$form = $this->form_builder->create_form();
		
		if ($form->validate())
		{
            //get the form data
            $name = $this->input->post('name');
            $from_email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');

            //set to_email id to which you want to receive mails
            $to_email = $this->config->item('admin_email', 'ion_auth');
			
		 	$this->email->from($from_email, $name);     
		 	$this->email->to($to_email);     
		 	//$this->email->cc('another-destination@email.com');    
		 	//$this->email->bcc('blinded-destination@email.com');      
		 	$this->email->subject($subject);     
		 	$this->email->message($message);      
		 	//$this->email->send();
		 
		 if ($this->email->send())
            {
                // mail sent
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
                redirect('contact/index');
            }
            else
            {
                //error
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is error in sending mail! Please try again later</div>');
                redirect('contact/index');
            }//end if
			
		}
		
		// require reCAPTCHA script at page head
		$this->mScripts['head'][] = 'https://www.google.com/recaptcha/api.js';
		
		$this->mPageTitle = "Contact us";	
		// display form
		$this->mViewData['form'] = $form;
		$this->render('contact/contact', 'member');
		  
	}
}