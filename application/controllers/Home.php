<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		// CI Bootstrap libraries
		$this->load->library('form_builder');
		$this->load->library('system_message');
		$this->load->library('email_client');

		$this->push_breadcrumb('Auth');
		$this->load->model('search_model');
		$this->load->library("pagination");
		$this->load->helper('text');
		$this->session->unset_userdata('searchterm');
		$this->load->model('user_model');
	}

	public function index()
	{
		$user_id = $this->session->userdata('user_id');
		$userDetails = $this->user_model->get_user_by_id($user_id, '1');
		$userName = $userDetails->username;
				
		if ($this->ion_auth->logged_in()){
			redirect('user/'.urldecode(url_title($userName)));
		}
		
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('email');
			$password = $this->input->post('password');
			$additional_data = array(
				'username'	=> $this->input->post('username'),
				//'group_id'		=> $this->input->post('group_id'),
			);

			// create user (default group as "business")
			$user = $this->ion_auth->register($identity, $password, $identity, $additional_data);
			if ($user)
			{	
				// send email using Email Client library
				if ($this->config->item('email_activation', 'ion_auth') && !$this->config->item('use_ci_email', 'ion_auth'))
				{
					$subject = $this->lang->line('email_activation_subject');
					$email_view = $this->config->item('email_templates', 'ion_auth').$this->config->item('email_activate', 'ion_auth');
					$this->email_client->send($identity, $subject, $email_view, $user);
				}

				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect('auth/login');
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}
		
		// require reCAPTCHA script at page head
		$this->mScripts['head'][] = 'https://www.google.com/recaptcha/api.js';

		// display form
		$this->mViewData['form'] = $form;
		$this->render('home', 'full_width');
		
		//$this->render('home', 'full_width');
	}
	
	public function search($searchterm, $country)
	{ 
		$country = $this->input->get_post('location', TRUE);
		$searchterm = $this->search_model->searchterm_handler($this->input->get_post('searchterm', TRUE));		
		$sql = "SELECT a.username, a.email, a.phone, a.created_on, b.* FROM users a LEFT JOIN business_profiles b ON b.user_id = a.id WHERE a.username LIKE '%$searchterm%' AND b.country LIKE '%$country%'";
		$q = $this->db->query($sql);
		$this->mViewData['numofRow'] = $q->num_rows();
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$data[] = $row;
			}
			/*echo "<pre>";
			print_r($data);
			echo "</pre>";*/
			//return $data;
		}
		
		
		$this->mPageTitle = "Search";		
		$this->mViewData['data'] = $data;
		$this->render('pages/search', 'default');
	}
}
// Refrence
//SELECT a.*,b.*,C.* FROM users a LEFT JOIN products b ON b.user_id = a.id LEFT JOIN product_types c ON b.product_type = c.id WHERE a.company_name LIKE '%$string%' OR a.address_1 LIKE '%$string%' OR a.city LIKE '%$string%' a.contact LIKE '%$string%' b.name LIKE '%$string%'