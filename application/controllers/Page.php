<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// only login users can access Account controller
		//$this->verify_login();
		$this->load->model('page_model');
	}

	public function index($page_name){
		$page_details = $this->page_model->get_page($page_name);
		$this->mViewData['page_details'] = $page_details;
		
		$this->mPageTitle = $page_details->name;		
		$this->render('pages/content', 'default');
	}
	
	
	
}