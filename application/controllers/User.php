<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// only login users can access Account controller
		$this->verify_login();
		$this->load->model('user_model');
		$image = '';
	}


	public function index($usrname)	{
			
		//$edate=strtotime($_POST['edate']); 
		//print_r($_REQUEST);

		$Specilized_category = $this->input->post('payment');    	
    	$payment = implode(',', $Specilized_category);
    	//print_r($payment);
		//exit;		

		$user_id = $this->session->userdata('user_id');		
		$userDetails = $this->user_model->get_user_by_id($user_id, '1');
		$userName = $userDetails->username;
		$businessDetails = $this->user_model->get_businessprofile_by_id($user_id);
		$this->mViewData['userDetails'] = $userDetails;	
		$this->mViewData['category'] = $this->user_model->get_category($businessDetails);
			

		//$info = $this->db->select()->from('users')->where('id', $user_id)->get()->row();
		$info = $this->db->query("SELECT username, email FROM users WHERE id = ".$user_id)->row();		
		if($info->username != $this->input->post('businessname')) {
	       $is_unique_username =  '|is_unique[users.username]';
	    } else {
	       $is_unique_username =  '';
	    }

	    if($info->email != $this->input->post('email')) {
	       $is_unique_email =  '|is_unique[users.email]';
	    } else {
	       $is_unique_email =  '';
	    }

		//validate form input		

		$this->form_validation->set_rules('businessname','Business Name','trim|required|xss_clean'.$is_unique_username);
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean'.$is_unique_email);
		$this->form_validation->set_rules('website','Website','trim|valid_url_format|url_exists|xss_clean');
		$this->form_validation->set_rules('phonenumber','Phone Number','trim|required|xss_clean');			
		$this->form_validation->set_rules('city','City','trim|required|xss_clean');
		$this->form_validation->set_rules('state','State','trim|required|xss_clean');
		$this->form_validation->set_rules('country','Country','trim|required|xss_clean');
		$this->form_validation->set_rules('zipcode','zipcode','trim|required|xss_clean');
		$this->form_validation->set_rules('address1','Address# 1','trim|required|xss_clean');
		//$this->form_validation->set_rules('address2','Address# 2','trim|required|xss_clean');
		$this->form_validation->set_rules('bcategories','Business Category','trim|required|xss_clean');
		$this->form_validation->set_rules('nemploye','Number of Employees','trim|required|xss_clean');
		$this->form_validation->set_rules('bprofile','Business Profile','trim|required|xss_clean');
		$this->form_validation->set_rules('bprofile','Business Profile','trim|required|xss_clean');
		$this->form_validation->set_rules('accept_currency','Accepted Currency','trim|required|xss_clean');
		$this->form_validation->set_rules('edate','Business established date','trim|required|xss_clean');
		$this->form_validation->set_rules('payment[]','Payment Method','trim|required|xss_clean');

		if (!empty($_FILES['profile_img_path']['name'])) {			
			$this->form_validation->set_rules('profile_img_path', 'profile img', 'callback_image_upload');
		}	

		//Change Delimiters				
		$this->form_validation->set_error_delimiters('<label class="control-label">', '</label>');
	
		if ($this->form_validation->run($this) == true)
		{
			
			if($this->input->post('accept') == 'on'){
				$checkon = "1";
			}
			$imageName = $this->session->userdata('imageName');			

			$data = array(		
				'username' 	=> $this->input->post('businessname'),					
				'phone' 	=> $this->input->post('phonefull'),
				'email' 	=> $this->input->post('email'),
			);			
			$dataPro = array(						
				'city' 			=> $this->input->post('city'),
				'state' 		=> $this->input->post('state'),
				'website' 		=> $this->input->post('website'),
				'country' 		=> $this->input->post('country'),
				'zipcode' 		=> $this->input->post('zipcode'),
				'address' 		=> $this->input->post('address1'),
				'address2'		=> $this->input->post('address2'),	
				'business_category' => $this->input->post('bcategories'),
				'company_size' 	=> $this->input->post('nemploye'),
				'description' 	=> $this->input->post('bprofile'),
				'accept' 		=> $checkon,	
				'last_modified'	=> time(),		
				'brnumber'		=> $this->input->post('brnumber'),
				'accept_currency' => $this->input->post('accept_currency'),
				'edate'			=> strtotime($this->input->post('edate')),
				'payment'		=> $payment,	
				//'next'			=> '1'					
			);
			if (!empty($_FILES['profile_img_path']['name'])) {
				$dataPro = array(						
					'profile_img_path' => $imageName,
				);
			}
//echo strtotime($this->input->post('edate'));
//echo "<pre>";
			//print_r($dataPro);exit;
			
			$this->user_model->update_user($user_id, $data);
			$this->user_model->update_business_profile($user_id, $dataPro);
			$this->session->set_flashdata('success', "Profile update successfully.");	
			$this->session->unset_userdata('imageName');
			
			redirect(base_url().'user/'.urldecode(url_title($userName)).'/?update');	
		}else{

			$this->mViewData['message'] = (validation_errors());
			$this->mViewData['businessDetails'] = $businessDetails;
			$this->mViewData['businessname'] = array(		
				'name' => 'businessname',		
				'id' => 'businessname',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('businessname', $userDetails->username),		
			);

			$this->mViewData['email'] = array(		
				'name' => 'email',	
				'type' => 'email',	
				'id' => 'email',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('email', $userDetails->email),		
			);

			$this->mViewData['website'] = array(		
				'name' => 'website',		
				'id' => 'website',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('website', $businessDetails->website),		
			);

			$this->mViewData['phonenumber'] = array(		
				'name' => 'phonenumber',		
				'id' => 'phonenumber',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('phonenumber', $userDetails->phone),		
			);

			$this->mViewData['zipcode'] = array(		
				'name' => 'zipcode',		
				'id' => 'zipcode',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('zipcode', $businessDetails->zipcode),		
			);

			$this->mViewData['address1'] = array(		
				'name' => 'address1',		
				'id' => 'address1',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('address1', $businessDetails->address),		
			);

			$this->mViewData['address2'] = array(		
				'name' => 'address2',		
				'id' => 'address2',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('address2', $businessDetails->address2),		
			);

			$this->mViewData['city'] = array(		
				'name' => 'city',		
				'id' => 'city',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('city', $businessDetails->city),		
			);

			$this->mViewData['state'] = array(		
				'name' => 'state',		
				'id' => 'state',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('state', $businessDetails->state),		
			);

			/*$this->mViewData['nemploye'] = array(		
				'name' => 'nemploye',		
				'id' => 'nemploye',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('nemploye', $businessDetails->company_size),		
			);*/

			$this->mViewData['brnumber'] = array(		
				'name' => 'brnumber',		
				'id' => 'brnumber',							
				'class' => 'form-control',	
				'value' => $this->form_validation->set_value('brnumber', $businessDetails->brnumber),		
			);

			$this->mViewData['bprofile'] = array(		
				'name' => 'bprofile',		
				'id' => 'bprofile',							
				'class' => 'form-control',	
				'rows' => 12,
				'cols' => 50,
				'value' => $this->form_validation->set_value('bprofile', $businessDetails->description),		
			);

			/*$this->mViewData['accept'] = array(		
				'name' => 'accept',		
				'id' => 'accept',											
				'value' => $this->form_validation->set_value('accept', $businessDetails->accept),		
			);*/
		
		}

		$this->mPageTitle = $userDetails->username.' - enquirehub';		
		$this->mViewData['user'] = $this->mUser;
		$this->render('users/business', 'full_width');
	}

	public function image_upload(){		
		  if($_FILES['profile_img_path']['size'] != 0){		  	
			$upload_dir = 'assets/uploads/profile/';
			if (!is_dir($upload_dir)) {
				 mkdir($upload_dir, 0777, TRUE);
			}	
			$config['upload_path']   = $upload_dir;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['file_name']     = 'profile_img_'.substr(md5(rand()),0,7);
			$config['overwrite']     = FALSE;
			$config['max_size']	 	 = '5124';
	
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('profile_img_path')){
				$this->form_validation->set_message('image_upload', $this->upload->display_errors());
				$this->upload->display_errors();
				return FALSE;
			}	
			else{				 			
				$data = array('upload_data' => $this->upload->data());
				$image = $data['upload_data']['file_name'];
				$newData = array('imageName' => $image , );				
				$imageName = $this->session->set_userdata($newData);				
        		return TRUE;
			}	
		}	
		else{
			$this->form_validation->set_message('image_upload', "No file selected");
			return FALSE;
		}
	}

	
}