<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search_Model extends MY_Model
{
	public function search_record_count($searchterm)
	{
		$sql = "SELECT COUNT(*) As cnt FROM users
				WHERE username LIKE '%" . $searchterm . "%'";
		$q = $this->db->query($sql);
		$row = $q->row();
		return $row->cnt;
	}
	
	public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata('searchterm', $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata('searchterm'))
		{
			$searchterm = $this->session->userdata('searchterm');
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

}

/* End of file Search_model.php */
/* Location: ./application/models/search_model.php */