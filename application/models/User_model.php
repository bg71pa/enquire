<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class User_Model extends MY_Model
{
	private $table_name			= 'users';			// user accounts
	private $profile_table_name	= 'user_profiles';	// user profiles

	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->table_name			= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		$this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
	}

	/**
	 * Get user record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_user_by_id($user_id, $activated)
	{
		$this->db->where('id', $user_id);
		$this->db->where('active', $activated ? 1 : 0);

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by login (username or email)
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_login($login)
	{
		$this->db->where('LOWER(username)=', strtolower($login));
		$this->db->or_where('LOWER(email)=', strtolower($login));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by username
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_username($username)
	{
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email($email)
	{
		$this->db->where('LOWER(email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Get user profile record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_userprofile_by_id($user_id)
	{
		$this->db->where('user_id', $user_id);		

		$query = $this->db->get($this->profile_table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get business profile record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_businessprofile_by_id($user_id)
	{
		$this->db->where('user_id', $user_id);		

		$query = $this->db->get('business_profiles');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user profile update record by user_id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	public function update_user_profile($user_id, $dataPro)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update($this->profile_table_name, $dataPro);
	}

	public function update_business_profile($user_id, $dataPro)
	{
		$this->db->where('user_id', $user_id);
		$this->db->update('business_profiles', $dataPro);
	}

	public function update_user($user_id, $data)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, $data);
	}
	
	
	function first_login($user_id, $data){
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, $data);		
	}		
	
	function categories_data()
	{
		$this->db->from('categories');
		//$this->db->order_by('category_name');
		$this->db->where('status', '1');
		$result = $this->db->get();
		$return = array();
		if($result->num_rows() > 0) {
			foreach($result->result_array() as $row) {
			$return[$row['id']] = $row['category_name'];
		}	
	}
	
			return $return;
	
	}
	
	public function get_user_info($username){
		//$sql = "SELECT a.username, a.email, a.phone, a.created_on, b.* FROM users a LEFT JOIN business_profiles b ON b.user_id = a.id WHERE a.username = '$username'";
		$this->db->select('users.username, users.email, users.phone, users.created_on, users.last_login, business_profiles.*');
		$this->db->from('users');
		$this->db->join('business_profiles', 'users.id = business_profiles.user_id');
		$this->db->where('users.username', $username);
		
		$query = $this->db->get();
		return $query->row();
		//echo $this->db->last_query();
	}

	//Used in Profile controller and has data from above function passed into it. 
	public function get_category($data)
	{
		$sql = "SELECT * FROM categories WHERE id = ". $data->business_category;
		$query = $this->db->query($sql);

		$row = $query->row();

		return $row;
	}
	
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */