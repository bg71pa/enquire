<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('categories');
		
		//$crud->set_theme('flexigrid');
		//$crud->set_table('categories');
		$crud->set_subject('Categories');
		$crud->required_fields('category_name');
		$crud->columns('category_name', 'status', 'date_created','date_updated');

		//$crud->set_field_upload('image','assets/uploads/files');

		$crud->callback_before_insert(array($this, 'd'));
		$crud->callback_before_update(array($this, '_modified_callback'));

		$crud->change_field_type('date_created','invisible');
		$crud->change_field_type('date_updated','invisible');
		$crud->change_field_type('cat_name_slug','invisible');
		$crud->change_field_type('description','invisible');
		$crud->change_field_type('image','invisible');
		$crud->change_field_type('created_from_ip','invisible');
		$crud->change_field_type('updated_from_ip','invisible');

		$this->mPageTitle = 'Categories';
		$this->render_crud();
	}
	
	function d($post_array) {
		$post_array['cat_name_slug'] = url_title($post_array['category_name'], 'dash', TRUE);
		$post_array['date_created'] = date('Y-m-d H:i:s');
		$post_array['date_updated'] = date('Y-m-d H:i:s');
		$post_array['created_from_ip'] = $this->input->ip_address();
		$post_array['updated_from_ip'] = $this->input->ip_address();
		return $post_array;
	}
	
	function _modified_callback($post_array) {		
		$post_array['cat_name_slug'] = url_title($post_array['category_name'], 'dash', TRUE);		
		$post_array['date_updated'] = date('Y-m-d H:i:s');
		$post_array['updated_from_ip'] = $this->input->ip_address();
		return $post_array;
	}
	
}