<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mTitle = 'Content - ';
		$this->push_breadcrumb('Content');
	}

	public function index()
	{
		redirect('admin/content/page');
	}

	// Grocery CRUD - Pages
	public function page()
	{
		$crud = $this->generate_crud('tbl_pages');		
		$crud->required_fields('name', 'description');
		
		$crud->columns('name', 'description', 'create_date', 'last_modified', 'status');
		$crud->fields('name','slug', 'description', 'create_date', 'last_modified', 'status');
		
		$crud->set_subject('Pages');
		
		$this->mTitle.= 'Pages';
		
		$crud->callback_before_insert(array($this, '_c_callback'));
		$crud->callback_before_update(array($this, '_modified_callback'));
	
		$crud->change_field_type('create_date','invisible');
		$crud->change_field_type('last_modified','invisible');
		$crud->change_field_type('slug','invisible');
		
		//$crud->add_action('Smileys', 'http://www.grocerycrud.com/assets/uploads/general/smiley.png', 'content/action_smiley');
		
		$this->render_crud();
	}
	
	function _c_callback($post_array) {
		$post_array['slug'] = url_title($post_array['name'], 'dash', TRUE);		
		$post_array['create_date'] = date('Y-m-d H:i:s');
		$post_array['last_modified'] = date('Y-m-d H:i:s');
		return $post_array;
	}
	
	function _modified_callback($post_array) {		
		$post_array['slug'] = url_title($post_array['name'], 'dash', TRUE);
		$post_array['last_modified'] = date('Y-m-d H:i:s');
		return $post_array;
	}
	
	/*function action_smiley($id)
    {
        echo "<div style='font-size:16px;font-family:Arial'>";
        echo "Just a test function for action button smiley and id: <b>".(int)$id."</b><br/>";
        echo "<a href='".site_url('content/pages')."'>Go back to example</a>";
        echo "</div>";
        die();
    }*/
	
	
}