<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $base_url; ?>" />
    <meta name="description" content=" An expanding online directory, enquirehub aims to connect businesses on a global platform"> 
	<meta name="keywords" content="enquirehub, Enquire Hub, global, online business, enquire, Business, networking, b2b, Business Directory, promote your business, social network for business, enquiries, contact business, message business, businesses, business information, international, business profile, industry, sector, business category, directory, promote, market your business, business marketing, marketing , business hub, network, inquire, inquire hub."> 

	<meta name="author" content="<?php echo $base_url; ?>">	

	<title><?php echo $page_title; ?></title>
	<link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon">

	<link rel="icon" type="image/gif" href="<?= base_url(); ?>favicon.gif" />
	<link rel="icon" type="image/png" href="<?= base_url(); ?>favicon.png" />

	<?php
		foreach ($meta_data as $name => $content)
		{
			if (!empty($content))
				echo "<meta name='$name' content='$content'>".PHP_EOL;
		}

		foreach ($stylesheets as $media => $files)
		{
			foreach ($files as $file)
			{
				$url = starts_with($file, 'http') ? $file : base_url($file);
				echo "<link href='$url' rel='stylesheet' media='$media'>".PHP_EOL;	
			}
		}
		
		foreach ($scripts['head'] as $file)
		{
			$url = starts_with($file, 'http') ? $file : base_url($file);
			echo "<script src='$url'></script>".PHP_EOL;
		}
	?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- remove this if you use Modernizr -->
	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<?php $is_home = ($this->router->fetch_class() === 'home') ? true : false; ?>
<body class="<?php if($is_home == TRUE){?><?php echo "home"; }?> <?php echo $body_class; ?> <?php if($this->ion_auth->logged_in()){ echo "login";}?>">
	<?php include("./application/views/analyticstracking.php") ?>