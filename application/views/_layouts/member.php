<?php $this->load->view('_partials/navbar'); ?>

<div class="container">
    <div class="row">
	<section class="content login-text col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<?php $this->load->view($inner_view); ?>
	</section>
    </div>
</div>

<?php $this->load->view('_partials/footer'); ?>