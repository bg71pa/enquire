<?php $is_home = ($this->router->fetch_class() === 'home') ? true : false; ?>
<?php if($is_home != TRUE){?></section><?php }?>
<div class="footer">
	<?php 
        if(!$this->ion_auth->logged_in()){
          if($is_home == TRUE){
            /*
  ?>
  <div id="wrapper5">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="footer-text">
              <h1><img src="<?= base_url();?>/assets/dist/images/logo_dark.png" class="img-responsive" alt=""></h1>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="footer-text">
              <h3>about us</h3>
              <ul>
                                
                <li><a href="<?= base_url();?>auth/login/">Login/Sign Up</a></li>
                <li><a href="<?= base_url();?>">Link text</a></li>
                <li><a href="<?= base_url();?>">Link text</a></li>
                <li><a href="<?= base_url();?>">Link text</a></li>
                <li><a href="<?= base_url();?>">Link text</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="footer-text">
              <h3>PARTNERS &amp; PRESS</h3>
              <ul>
                <li><a href="<?= base_url();?>">Link text</a></li>
                                <li><a href="<?= base_url();?>">Link text</a></li>
                              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="footer-text">
              <h3>connect with us</h3>
            </div>
            <div class="small-icon">
              <ul>
                <li><a><i class="fa fa-phone"></i><strong>000-000-0000</strong></a></li>
                <li><a href="mailto:test@test.com"><i class="fa fa-envelope-o"></i>test@test.com</a></li>
              </ul>
            </div>
                        <div class="social_icon1">
              <ul>
                <li class="no-border"><a href="#" class="facebook "><i class="fa fa-facebook"></i></a></li>
                <li class="no-border"><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="no-border"><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

  <?php */}}?>
    
    <div id="wrapper6" class="fbtm">
      <div class="container">
        <div class="row">
          <div class="col-md-8 flink">
            <a href="<?php echo base_url();?>page/about">About</a>
            <a href="<?php echo base_url();?>contact">Contact us</a>
            <a href="<?php echo base_url();?>page/user-agreement">User Agreement</a>
            <a href="<?php echo base_url();?>page/privacy-policy">Privacy Policy</a>
            <a href="<?php echo base_url();?>page/cookie-policy">Cookie Policy</a>
          </div>
          <div class="col-md-4">
            <div class="text-copy">
              <p><?php echo date('Y'); ?> © <a href="<?= base_url();?>">enquirehub</a> - Bringing Businesses Together</p>
            </div>
          </div>
        </div>
      </div>
    </div>
        	
	<div class="container">
		<?php if (ENVIRONMENT=='development'): ?>
			<p class="pull-right text-muted">
				CI Bootstrap Version: <strong><?php echo CI_BOOTSTRAP_VERSION; ?></strong>, 
				CI Version: <strong><?php echo CI_VERSION; ?></strong>, 
				Elapsed Time: <strong>{elapsed_time}</strong> seconds, 
				Memory Usage: <strong>{memory_usage}</strong>
			</p>
		<?php endif; ?>		
	</div>
</div>