<?php $is_home = ($this->router->fetch_class() === 'home') ? true : false; ?>
<?php //echo $is_home; print_r($_SESSION);?>

<header id="head" class="main-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"> <a href="<?= base_url();?>" class="logo"><img src="<?= base_url();?>assets/dist/images/enquirehub_logo.png" alt="enquirehub"></a> </div>
      <div class="col-sm-8">
        <div class="sidenav">
          
          <?php 
			  if(!$this->ion_auth->logged_in()){
				  if($is_home == TRUE){
		  ?>
              <form class="form-inline form3" action="<?= base_url();?>auth/login" method="post" accept-charset="utf-8" id="signIn">
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                  <a href="<?= base_url();?>auth/forgot_password" class="fgclass">Forgotten password?</a> </div>
                <button type="submit" class="btn btn-default">Log In</button>
              </form>
              <?php } //if home 
			  	else{
			  ?>
              <div class="pull-right pt10"> <a href="<?= base_url();?>auth/login" class="btn btn-primary">Log in</a> <a href="<?= base_url();?>auth/sign_up" class="btn btn-primary">Sign up</a> </div>
              <?php } // end if else home?>
          <?php }else{ ?>
		  	<div class="pull-right pt10">                        
            <nav class="navbar navbar-static-top">
      
              <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">                  
                  <!-- User Account: style can be found in dropdown.less -->
                  <?php 
                    if( $businessDetails->profile_img_path != ''){
                      $imageUrl = base_url()."assets/uploads/profile/".$businessDetails->profile_img_path; 
                    }else{
                      $imageUrl = base_url()."assets/dist/images/pro.png";
                    } 
                  ?>
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <img src="<?= $imageUrl; ?>" class="user-image" alt="<?php echo $businessDetails->username;?>">
                      <span class="hidden-xs"><?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->username;} ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header dflex">                        
                        <img src="<?= $imageUrl; ?>" class="img-square dflex" alt="<?php echo $businessDetails->username;?>">
        
                        <div class="dflex">
                          <p>
                          <?php if($user->first_name != '') {echo $user->first_name.' '.$user->last_name;} else { echo $user->username;} ?>
                          <small><?php echo date('M d, Y', $userDetails->created_on);?></small>
                          </p>
                          <div>
                            <a href="<?= base_url();?>profile/<?= urldecode(url_title($user->username));?>" class="btn btn-default btn-flat">View my profile</a>
                          </div>
                        </div>                        
                      </li>
                      <!-- Menu Body -->
                      

                      <li>                        
                        <a href="<?= base_url();?>user/<?= urldecode(url_title($user->username));?>" class="abtn">Edit Profile</a>
                        <a href="<?= base_url();?>auth/change_password" class="abtn">Change Password</a>
                        <a href="<?= base_url();?>auth/logout" class="abtn">Log out</a>
                      </li>
                    </ul>
                  </li>
                  <!-- Control Sidebar Toggle Button -->                
                </ul>
              </div>
        
            </nav>
            
            <?php /*?><a href="<?= base_url();?>auth/logout" class="btn btn-primary">Logout</a><?php */?> </div>
		  <?php }?>
        </div>
      </div>
    </div>
  </div>
</header>
<?php if($is_home != TRUE){?><section id="midarea"><?php }?>
<?php /*?><nav class="navbar navbar-default" role="navigation">
<div class="container">

	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href=""><?php echo $site_name; ?></a>
	</div>

	<div class="navbar-collapse collapse">

		<ul class="nav navbar-nav">
			<?php foreach ($menu as $parent => $parent_params): ?>

				<?php if (empty($parent_params['children'])): ?>

					<?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
					<li <?php if ($active) echo 'class="active"'; ?>>
						<a href='<?php echo $parent_params['url']; ?>'>
							<?php echo $parent_params['name']; ?>
						</a>
					</li>

				<?php else: ?>

					<?php $parent_active = ($ctrler==$parent); ?>
					<li class='dropdown <?php if ($parent_active) echo 'active'; ?>'>
						<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
							<?php echo $parent_params['name']; ?> <span class='caret'></span>
						</a>
						<ul role='menu' class='dropdown-menu'>
							<?php foreach ($parent_params['children'] as $name => $url): ?>
								<li><a href='<?php echo $url; ?>'><?php echo $name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>

				<?php endif; ?>

			<?php endforeach; ?>
		</ul>

		<?php //$this->load->view('_partials/language_switcher'); ?>
		
	</div>

</div>
</nav><?php */?>
<?php //print_r($_SESSION);?>