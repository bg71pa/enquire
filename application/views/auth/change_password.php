<div class="page-header"><h1><?php echo $page_title; ?></h1><p>change your account password from here.</p></div>
<?php 
      $email  = '';
      if(isset($_SESSION['form-1'])){ 
            $email = $_SESSION['form-1']['email']; 
      }
?>
<?php echo $form->open(); ?>
      <?php echo $form->messages(); ?>
      
      <?php echo $form->bs3_password('Old Password', 'old'); ?>
      <?php echo $form->bs3_password('New Password', 'new'); ?>
      <?php echo $form->bs3_password('Confirm New Password', 'new_confirm'); ?>
      <?php echo form_hidden('_hidden_field', $user->id);?>

      <?php echo $form->bs3_submit('Change Password'); ?>

<?php echo $form->close(); ?>