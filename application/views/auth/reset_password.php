<div class="page-header"><h1><?php echo $page_title; ?></h1><p>PLEASE FILL OUT THE FORM TO RESET PASSWORD</p></div>

<?php echo $form->open(); ?>
	
	<?php echo $form->messages(); ?>
	
	<?php echo $form->bs3_password('Password', 'password'); ?>
	<?php echo $form->bs3_password('Retype Password', 'retype_password'); ?>
	<?php echo $form->bs3_submit(); ?>
	
<?php echo $form->close(); ?>