<div class="page-header"><h1><?php echo $page_title; ?></h1><p>PLEASE FILL OUT THE FORM TO SIGN UP</p></div>

<?php $attributes = array('class' => 'email', 'id' => 'myform'); echo $form->open(); ?>
<?php /*?><form action="<?= base_url();?>auth/sign_up" class="formr" id="signupForm" method="post" accept-charset="utf-8"><?php */?>
	
	<?php echo $form->messages(); ?>
	<div class="form-group">
                                  <?php /*?><div class="radio radio-info radio-inline">                                    
                                      <input type="radio" name="group_id" id="optionsRadios1" value="1" checked="">
                                      <label for="optionsRadios1">Register business </label>
                                  </div>
                                  <div class="radio radio-info radio-inline">                                        
                                      <input type="radio" name="group_id" id="optionsRadios2" value="2">
                                      <label for="optionsRadios2">I am an individual </label>
                                  </div><?php */?>
                                </div>
	<?php echo $form->bs3_text('Business name', 'username', '', array('placeholder'=>'Business name')); ?>	
	<?php echo $form->bs3_email('Email', 'email', '', array('placeholder'=>'Email')); ?>
	<?php echo $form->bs3_password('Password', 'password', '', array('placeholder'=>'Password')); ?>
	<?php echo $form->bs3_password('Confirm password', 'retype_password', '', array('placeholder'=>'Confirm password')); ?>
	<p><?php echo $form->field_recaptcha(); ?></p>

	<div class="form-group">
		Have an account? <a href="auth/login">Log in</a>
	</div>
	
	<?php echo $form->bs3_submit('Sign up Now!'); ?>
    <div class="fm warning">By clicking signup now, you agree to enquirehub’s <strong><a href="<?php echo base_url(); ?>page/user-agreement">User Agreement</a>, <a href="<?php echo base_url(); ?>page/privacy-policy">Privacy Policy</a>,</strong> and <strong><a href="<?php echo base_url(); ?>page/cookie-policy">Cookie Policy</a></strong>.</div>

<?php echo $form->close(); ?>