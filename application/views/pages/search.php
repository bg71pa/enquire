<section id="midarea">
    <div class="bloombox">
    <?php //print_r($_REQUEST); 
    $this->session->unset_userdata('searchterm');?>
    <?php if($_REQUEST['searchterm'] != '') {?>
    <div class="container">
        <hgroup class="mb20">
            <h1>Search Results</h1>
            <h2 class="lead"><strong class="text-danger"><?php echo $numofRow;?></strong> results were found for the search for <strong class="text-danger"><?php echo $_REQUEST['searchterm'];?></strong></h2>								
        </hgroup>
    
        <section>
            <?php
                /*echo "<pre>";
                        print_r($data);
                        echo "</pre>";*/
                foreach($data as $datarow){
            ?>
            <article class="search-result row">
            	<?php 
				  if($datarow->profile_img_path != ''){
					$imageUrl = base_url()."assets/uploads/profile/".$datarow->profile_img_path; 
				  }else{
					$imageUrl = base_url()."assets/dist/images/pro.png";  
				  }
				?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a href="#" title="Lorem ipsum" class="thumbnail"><img src="<?php echo $imageUrl;?>" alt="Lorem ipsum" /></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <ul class="meta-search">
                        <li><i class="glyphicon glyphicon-calendar"></i> <span><?php echo date('M d, Y', $datarow->created_on);?></span></li>
                        <li><i class="glyphicon glyphicon-time"></i> <span>4:28 pm</span></li>
                        <li><i class="glyphicon glyphicon-tags"></i> <span>People</span></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-7 excerpet">
                    <h3><a href="<?php echo base_url()."profile/".$datarow->username;?>" title=""><?php echo $datarow->username;?></a></h3>
                    <p><?php echo word_limiter($datarow->description, 45);?></p>						
                    <span class="plus"><a href="#" title="Lorem ipsum"><i class="glyphicon glyphicon-plus"></i></a></span>
                </div>
                <span class="clearfix border"></span>			
            </article>        			
            <?php } ?>            
        </section>
     </div>   
        <?php if ($numofRow == 0){?>            	
            <div class="midbottom">
                    <div class="searchbusiness">
                        <div class="container">            
                            <h1>Find A Business</h1>
                            <form action ="<?=base_url()?>home/search" method="get" id="searchform" class="form-inline form4">
                              <div class="form-group sbox">
                                <label class="sr-only" for="exampleInputEmail3">Email address</label>
                                <input type="text" class="form-control" name="searchterm" id="searchterm" value="<?=$searchterm?>" placeholder="Search for businesses">
                              </div>
                              <div class="form-group dbox">
                                <label class="sr-only" for="exampleInputPassword3">Password</label>
                                <select id="location" name="location" class="form-control">
                                    <option selected="selected" value="">Select Location</option>
                                </select>
                              </div>
                              
                              <input type="submit" class="btn btn-default" value="Search">
                            </form>
                        </div>
                    </div>
                </div>
        <?php }?>
        
    <?php } else {?>
    
        <p>No Result Found</p>
    <?php }?>    
    </div>
</section>