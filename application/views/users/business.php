<?php //echo $form->messages(); ?>
<div id="infoMessage"><?php echo $message;?></div>
<?php echo $this->session->flashdata('message'); ?> 
<div id="wrapper7">
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h1>Customise your profile page in 1 minute <small>Register your business with enquirehub.</small></h1>
      </div>
      <div class="account-box">
        <?php $user_id = $userDetails->id;?>
        <?php if($this->session->flashdata('success') != ''){?>
        <div class="col-md-12">
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo @$this->session->flashdata('success');?> </div>
        </div>
        <?php }?>
        <?php $attributes = array('class' => 'forms3', 'enctype'=>'multipart/form-data', 'id'=>'userprofile'); echo form_open("user/".urldecode(url_title($userDetails->username)), $attributes);?>
        <div class="white_background">          
          <div class="row">
            <div class="col-md-4 sidebar_box">
              <div class="row">
                <div class="form-group <?php if(form_error('profile_img_path') != ''){?>has-error<?php } ?>">
                  <div class="col-sm-12">
                    <div class="accoutn-img">
                      <?php /*?><p>Make your profile stand out more by adding a profile image!</p><?php */?>
                      <!--INPUT-->
                      <?php $imageUrl = base_url()."assets/uploads/profile/".$businessDetails->profile_img_path; 
                       if($businessDetails->profile_img_path != ''){
                       ?>
                      <div class="form-img text-center mgbt-xs-15"> <img src="<?= $imageUrl; ?>" class="pro avatar img-square"> </div>
                      <br />
                      <?php /*?><input name = "profile_img_path" type="file" class="input-xlarge custom-file-input" id = "profile_img_path" /> <?php */?>
                      <div class="box text-center">
                        <input type="file" name="profile_img_path" id="profile_img_path" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
                        <label for="profile_img_path"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                          <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                          </svg> Choose Picture&hellip;</strong></label>
                      </div>
                      <?php }else{ ?>
                      <div class="form-img text-center mgbt-xs-15"> <img src="<?php echo base_url()."assets/dist/images/pro.png"?>" class="avatar img-circle" alt="avatar"> </div>
                      <br>
                      <div class="box text-center">
                        <input type="file" name="profile_img_path" id="profile_img_path" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
                        <label for="profile_img_path"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                          <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                          </svg> Choose Picture&hellip;</strong></label>
                      </div>
                      <h5 class="logoline">Upload your business logo above</h5>
                      <hr>
                      <?php } ?>
                      <?php echo form_error('profile_img_path'); ?>
                      
                    </div>
                    <div>
                        <table class="table table-striped table-hover">
                          <tbody>
                            <tr>
                              <td style="width:60%;">Status</td>
                              <td><span class="label label-success">Active</span></td>
                            </tr>
                            <tr>
                              <td>User Rating</td>
                              <td><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i></td>
                            </tr>
                            <tr>
                              <td>Date Created</td>
                              <td><?php echo date('M d, Y', $userDetails->created_on);?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              <div class="adv_box_area"> 
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> 
                <!-- Profile Page Advert --> 
                <ins class="adsbygoogle"
           style="display:inline-block;width:336px;height:280px"
           data-ad-client="ca-pub-3732786407521019"
           data-ad-slot="5224869088"></ins> 
                <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script> 
              </div>
            </div>
            <div class="col-md-8 roxen_box">

              <div class="roxen-in-header">
                <?php /*?><h1>You are just one step away from awesomeness!</h1><?php */?>
                <h2>Business Information</h2>
                <hr />
              </div>
              <?php //echo "<pre>";print_r($userDetalis); echo "</pre>";?>
              <?php if($this->session->flashdata('message') != ''){?>
              <br />
              <div class="alert alert-danger" role="alert"> <?php echo @$this->session->flashdata('message');?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
              </div>
              <?php }?>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group <?php if(form_error('businessname') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business Name', $businessname['id'], array('class'=>'label_class required')); ?> <?php echo form_input($businessname);?> <?php echo form_error('businessname'); ?> </div>
                  <div class="form-group <?php if(form_error('email') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business Email', $email['id'], array('class'=>'label_class required')); ?> <?php echo form_input($email);?> <?php echo form_error('email'); ?> </div>
                  <div class="form-group <?php if(form_error('address1') != ''){?>has-error<?php } ?>"> <?php echo form_label('Address# 1', $address1['id'], array('class'=>'label_class required')); ?> <?php echo form_input($address1);?> <?php echo form_error('address1'); ?> </div>
                  <div class="form-group <?php if(form_error('address2') != ''){?>has-error<?php } ?>"> <?php echo form_label('Address# 2', $address2['id'], array('class'=>'label_class')); ?> <?php echo form_input($address2);?> <?php echo form_error('address2'); ?> </div>
                  <div class="form-group <?php if(form_error('city') != ''){?>has-error<?php } ?>"> <?php echo form_label('City', $city['id'], array('class'=>'label_class required')); ?> <?php echo form_input($city);?> <?php echo form_error('city'); ?> </div>
                  <div class="form-group <?php if(form_error('state') != ''){?>has-error<?php } ?>"> <?php echo form_label('State/Province/Region', $state['id'], array('class'=>'label_class required')); ?> <?php echo form_input($state);?> <?php echo form_error('state'); ?> </div>
                  <div class="form-group <?php if(form_error('zipcode') != ''){?>has-error<?php } ?>"> <?php echo form_label('Zip code/Postcode', $zipcode['id'], array('class'=>'label_class required')); ?> <?php echo form_input($zipcode);?> <?php echo form_error('zipcode'); ?> </div>
                  <div class="form-group <?php if(form_error('country') != ''){?>has-error<?php } ?>"> <?php echo form_label('Country', '', array('class'=>'label_class required')); ?> <?php echo country_dropdown('country', 'cont', 'form-control', $businessDetails->country, array('US','CA','GB'), '');?>
                    <?php /*?><select id="address-country" name="country" class="form-control"></select><?php */?>
                    <?php echo form_error('country'); ?> </div>
                  <div class="form-group <?php if(form_error('payment') != ''){?>has-error<?php } ?>"> <?php echo form_label('Preferred payment methods', $payment['id'], array('class'=>'label_class required')); ?>
                    <?php //echo form_input($zipcode);?>
                    <?php 
                        $paymentoption = explode(',' , rtrim($businessDetails->payment));                         
                      ?>
                    <div class="row paymentrow">
                      <div class="col-sm-4">
                        <ul>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox1" name="payment[]" <?php (in_array("cash", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="cash">
                              Cash </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox2" name="payment[]" <?php (in_array("invoice", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="invoice">
                              Invoice </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox3" name="payment[]" <?php (in_array("diners", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="diners">
                              Diners </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox4" name="payment[]" <?php (in_array("financing", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="financing">
                              Financing </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox5" name="payment[]" <?php (in_array("discover", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="discover">
                              Discover </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox6" name="payment[]" <?php (in_array("debit_card", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="debit_card">
                              Debit Card </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox7" name="payment[]" <?php (in_array("bank_wire", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="bank_wire">
                              Bank Wire </label>
                          </li>
                        </ul>
                      </div>
                      <div class="col-sm-4">
                        <ul>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox8" name="payment[]" <?php (in_array("cheque", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="cheque">
                              Cheque </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox9" name="payment[]" <?php (in_array("credit_cards", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="credit_cards">
                              Credit Cards </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox10" name="payment[]" <?php (in_array("mastercard", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="mastercard">
                              Mastercard </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox11" name="payment[]" <?php (in_array("google_checkout", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="google_checkout">
                              Google Checkout </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox12" name="payment[]" <?php (in_array("maestro", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="maestro">
                              Maestro </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox13" name="payment[]" <?php (in_array("money_order", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="money_order">
                              Money Order </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox14" name="payment[]" <?php (in_array("online_banking", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="online_banking">
                              Online Banking </label>
                          </li>
                        </ul>
                      </div>
                      <div class="col-sm-4">
                        <ul>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox15" name="payment[]" <?php (in_array("western_union", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="western_union">
                              Western Union </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox16" name="payment[]" <?php (in_array("applypay", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="applypay">
                              ApplyPay </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox17" name="payment[]" <?php (in_array("androidpay", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="androidpay">
                              AndroidPay </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox18" name="payment[]" <?php (in_array("visa", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="visa">
                              Visa </label>
                          </li>
                          <li>
                            <label>
                              <input type="checkbox" id="inlineCheckbox19" name="payment[]" <?php (in_array("paypal", $paymentoption)) ? print 'checked="checked"' : FALSE; ?> value="paypal">
                              PayPal </label>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <?php echo form_error('payment'); ?> </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group <?php if(form_error('phonenumber') != ''){?>has-error<?php } ?>"> <?php echo form_label('Phone Number', $phonenumber['id'], array('class'=>'label_class required')); ?> <?php echo form_input($phonenumber);?> <?php echo form_error('phonenumber'); ?>
                    <input id="hiddenphone" type="hidden" name="phonefull">
                  </div>
                  <div class="form-group <?php if(form_error('website') != ''){?>has-error<?php } ?>"> <?php echo form_label('Website', $website['id'], array('class'=>'label_class')); ?> <?php echo form_input($website);?> <?php echo form_error('website'); ?> </div>
                  <div class="form-group <?php if(form_error('bcategories') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business Category', $bcategories['id'], array('class'=>'label_class required')); ?>
                    <?php
                        $data_cat = array(
                        '0' => 'Select Business Type',                        
                        "1" => 'Sole Trader',
                        "2" => 'Limited Company',
                        "3" => 'Business Prtnership',
                        "4" => "I would also add PLC's etc -that's Public Limited Companies such as FTSE100 companies etc E.g. Tesco or M&S",
                        "5" => 'None for profit'
                        );
                        $attribute = array('class' => 'form-control');
                        //echo form_dropdown('bcategories', $data_cat, $businessDetails->business_category, $attribute);
            
                        echo form_dropdown('bcategories', $categories, $businessDetails->business_category, $attribute);
                      ?>
                    <?php echo form_error('bcategories'); ?> </div>
                  <div class="form-group <?php if(form_error('nemploye') != ''){?>has-error<?php } ?>"> <?php echo form_label('Number of Employees', $nemploye['id'], array('class'=>'label_class required')); ?>
                    <?php
                        $data_emp = array(
                          '' => 'Select number of employess',
                          '0' => 'Self-employed',                        
                          "1" => '1-10 employess',
                          "2" => '11-50 employess',
                          "3" => '51-200 employess',
                          "4" => '201-500 employess',
                          "5" => '501-1000 employess',
                          "6" => '1001-5000 employess',
                          "7" => '5001-10,000 employess',
                          "8" => '10,001+ employees'
                        );
                        $attribute1 = array('class' => 'form-control');
            
                        echo form_dropdown('nemploye', $data_emp, $businessDetails->company_size, $attribute1);
                      ?>
                    <?php //echo form_input($nemploye);?>
                    <?php echo form_error('nemploye'); ?> </div>
                  <div class="form-group <?php if(form_error('brnumber') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business Registration Number', $brnumber['id'], array('class'=>'label_class')); ?> <?php echo form_input($brnumber);?> <?php echo form_error('brnumber'); ?> </div>
                  <div class="form-group <?php if(form_error('accept_currency') != ''){?>has-error<?php } ?>"> <?php echo form_label('Accepted Currency', $accept_currency['id'], array('class'=>'label_class required')); ?>
                    <div class="bfh-selectbox bfh-currencies" data-name="accept_currency" data-currency="<?php if($businessDetails->accept_currency != ''){ echo $businessDetails->accept_currency;} else { echo "EUR";}?>" data-flags="true"></div>
                    <?php echo form_error('accept_currency'); ?> </div>
                  <div class="form-group <?php if(form_error('edate') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business established date', $edate['id'], array('class'=>'label_class required')); ?>
                    <div class='input-group date' id='datetimepicker10'>
                      <input type='text' name="edate" value="<?php echo date("dd/mm/YY", $businessDetails->edate);?>" class="form-control" />
                      <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"> </span> </span> </div>
                    <?php echo form_error('edate'); ?> </div>
                  <div class="form-group <?php if(form_error('bprofile') != ''){?>has-error<?php } ?>"> <?php echo form_label('Business Profile', $bprofile['id'], array('class'=>'label_class required')); ?> <?php echo form_textarea($bprofile);?> <?php echo form_error('bprofile'); ?> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 form-group <?php if(form_error('accept') != ''){?>has-error<?php } ?>">
                  <div class="checkbox checkbox-primary">
                    <?php //echo form_checkbox($accept);?>
                    <input type="checkbox" <?php if($businessDetails->accept == '1'){?> checked="checked" <?php }?> name="accept" id="accept">
                    <label for="accept" class="checkbox_b"> I hereby accept all terms and conditions.</label>
                    <?php echo form_error('accept'); ?> </div>
                </div>
              </div>
              <div class="row">
                <hr>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Update Business Information" class="btn btn-success">
                  </div>
                </div>                
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
